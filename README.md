KSP-JNSQ 1.8 Woeller-Installation

  I. VORWORT
===============================================================================

**_VIELEN DANK AN WOELLER UND VALIZOCKT FÜR DAS ZUSAMMENSTELLEN DER MOD-LISTE!_**

Innerhalb der Distribution findest du im Ordner "Links" die Download Links der benötigten Mods sortiert nach Funktion. Diese sollten in den ebenfalls in dieser Distribution vorhandenen "Files" Ordner
gespeichert werden. Die größte Mod ist JNSQ, es wird empfohlen, diese zu erst anzuschieben und parallel die anderen Mods herunter zu laden ;-)
 
- **Dateimanager / Explorer auf alphabetische Sortierung stellen**
- keine ModuleManager dlls aus anderen Mods kopieren
- beachte: einige Mods haben in der zip-Datei weitere Ordner, bevor man in den GameData-Ordner kommt
- bitte kopiere **niemals** den kompletten GameData Ordner aus der zip-Datei der Mod, Gründe:
	* einige Mods bringen teilweise ihre (veralteten) Abhängigkeiten mit
	* Abhängigkeiten dieser Installation sind alle durch die _00_Abhängigkeiten-Dateien_ in ihrer aktuellen Version abgedeckt
- kopiere immer nur den Haupt-Mod-Ordner _unterhalb_ des GameData-Ordners aus der jeweiligen zip-Datei, höchstens es wird anderweitig angewiesen, Beispiel:
	* KAS_vX.X.zip, hier nur den KAS-Ordner in den GameDate-Ordner deiner Installation kopieren, auf keinen Fall den CommunityCategoryKit-Ordner!
- wenn eine Mod in **II. INSTALLATION** nicht aufgeführt wird, ist nichts besonderes zu beachten

 II. INSTALLATION
===============================================================================

**00-Abhaengigkeiten**

Abweichend von der Woeller-Modliste ist:

- Firespitter

explizit aufgeführt.

**_Instruktionen_**
- CustomBarnKit
	* kein GameData-Ordner in der Zip, Mod-Ordner liegt direkt in der Zip
- FireSpitter
	* komplett kopieren
	* dann in GameData/Firespitter alles außer Plugins und Resources löschen
- ModularFlightIntegrator
	* kein GameData-Ordner in der Zip, Mod-Ordner liegt direkt in der Zip
- ModuleManager
	* die ModuleManager dll direkt in den GameData-Ordner kopieren
	
	
**10-VisuelleMods**

**_Instruktionen_**
- JNSQ
	* **wichtige Info aus dem Forum, grob ins Deutsche übersetzt:** Es wird dringend empfohlen, die settings.cfg im Root-Ordner der KSP Installation zu löschen. Du musst danach die Grafiksettings [Patsch78: alle Settings] 
	beim Start des Spiels erneut einstellen. Das wird aber keinen Schaden an aktuellen Save-Files verursachen.
	* nur den JNSQ-Ordner installieren
	* ihr solltet im Save unter Advanced -> CommNet Options den DSN Modifier auf 4 einstellen (ist aber kaum punktgenau möglich. 4.05 geht sicher auch :-P)
- RealPlume-Stock
	* nur den RealPlume-Stock Ordner aus der Zip kopieren

**20-QoLMods**

**_Instruktionen_**
- KerbalAlarmClock
	* Haupt-Mod-Ordner ist TriggerTech
	
**30-SpielMechanik**

**_Instruktionen_**
- ContractConfigurator
	* nur ContractConfigurator-Ordner kopieren
- Final_Frontier
	* Haupt-Mod-Ordner ist Nereid
- JNSQ_FinalFrontier
	* Haupt-Mod-Ordner ist Nereid, überschreiben, falls gefragt wird
- KAS
	* Nur KAS-Ordner installieren
- KIS
	* Nur KIS-Ordner installieren
- RationalResources_Extras
	* unbedingt erst mit RationalResources-x.y.z.zip weiter machen!
- RationalResources-x.y.z.zip
	* nur RationalResources-Ordner installieren
- RationalResources_Extras
	* RationalResourcesParts installieren
	* danach die RR_*.cfg Dateien in den RationalResources-Ordner kopieren
- UnKerballed_Start	
	* vor dem GameData-Ordner ist ein weiterer Ordner in der Zip

**40-PartMods**

**_Instruktionen_**
- ReStockPlus
	* Super! Jetzt installieren. :P
- USICore
	* nur UmbraSpaceIndustries installieren

**50-Diverse**

**_Instruktionen_**
- USITools
	* nur 000_USITools installieren
- UnkerballedReStockPatch, Info von Woeller:
	* Wer mit Unkerballed Start und Restock/Restock+ spielt wird im Moment festellen, dass die neuen kleinen SRB's nicht in den ersten R&D Nodes zu finden sind. Der Grund dafür ist, dass ReStock die eigenen kleinen SRB's nicht mehr lädt (seit 1.8.1) und Unkerballed Start die neuen Stock SRB's noch nicht konfiguriert. Das verhindert einen Start in einen neue unbemannte Karriere, da keine kleinen SRB's mehr vorhanden sind.
- AudioMufflerRedux CutoffPatch
	* OPTIONAL, beinhaltet ein Setting, damit im All die Engines ein wenig Restlautstärke besitzen
	* vorhandene Dateien überschreiben, wenn gefragt

III. Abschluss der Installation
===============================================================================

Da wir KSP-AVC installiert haben wird empfohlen, alle miniavc*.* -Dateien aus der Installation zu entfernen (dll und xml)

IV. Known issues
===============================================================================

T.B.D.